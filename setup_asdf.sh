#!/usr/bin/env bash
source /opt/asdf-vm/asdf.sh;

# Java
asdf plugin add java
asdf install java $(asdf latest java temurin-jre-17)
asdf global java $(asdf latest java temurin-jre-17)

# Python
asdf plugin add python
asdf install python latest:3
asdf global python system